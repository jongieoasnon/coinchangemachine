/*
 * This file covers the coin and bill acceptor
 * mainly for initializatin and counting of the dispensed coins
 */

const byte bill_acceptor_interrupt_pin = 2;
const byte coin_acceptor_interrupt_pin = 3;
int acceptors_relay_pin = 22, inserted_amount = 0, last_inserted_amount = 0;
long last_inserted_amount_millis = 0, amount_insert_completed_millis = 0, transaction_completed_millis = 0;


void bill_and_coin_acceptor_setup() {
  pinMode(acceptors_relay_pin, OUTPUT);
  pinMode(coin_acceptor_interrupt_pin, INPUT);
  pinMode(bill_acceptor_interrupt_pin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(bill_acceptor_interrupt_pin), bill_acceptor_count, FALLING);
  attachInterrupt(digitalPinToInterrupt(coin_acceptor_interrupt_pin), coin_acceptor_count, RISING);
  digitalWrite(acceptors_relay_pin, HIGH);
}

// bill acceptor gives 1 pulse for every 10pesos of inserted bill
// eg: 10 pulses for 100 pesos
void bill_acceptor_count() {
  if (page == 2) {
    inserted_amount += 10;
  }
}

// coint acceptor gives 1 pulse for every 1peso of the inserted coin
// eg: 5 pulses for 5 pesos
void coin_acceptor_count() {
  if (page == 2) {
    inserted_amount += 1;
  }
}

// turn on coin and bill acceptor when in page 2 and 3 only
void set_acceptors_relay() {
  digitalWrite(acceptors_relay_pin, !(page==2 || page==3));
}

// function for counting inserted amount called in the main loop
void listen_inserted_amount() {
  if (last_inserted_amount != inserted_amount) {
    last_inserted_amount_millis = current_millis;
    last_inserted_amount = inserted_amount;
  }
  if (current_millis - 300 > last_inserted_amount_millis) {
    if (inputted_amount.toInt() == inserted_amount) {
      // exact
      transition_page(6);
    } else if (inputted_amount.toInt() < inserted_amount) {
      // with change
      transition_page(5);
    }
  }
}
