/*
 * This file covers the LCD UI pages
 */

unsigned long lcd_timer;
int lcd_blink_interval = 500;
bool page1_click_any_to_continue_visible = false;

void lcd_setup() {
  lcd1.begin();
  lcd1.backlight();
  lcd2.begin();
  lcd2.backlight();
  lcd1.setCursor(1, 1);
  lcd2.setCursor(1, 1);
  lcd1.print("Coin Change Machine");
  lcd2.print("Coin Change Machine");
  lcd1.setCursor(8, 2);
  lcd2.setCursor(8, 2);
  lcd1.print("****");
  lcd2.print("****");
  lcd1.setCursor(0, 3);
  lcd2.setCursor(0, 3);
  lcd1.print("Click to continue...");
  lcd2.print("Click to continue...");
}

void clear_lcds() {
  lcd1.clear();
  lcd2.clear();
  delay(100);
}

// function that is being called every page transition
// this covers any page initializations
void page_init(int page) {
  switch(page) {
    case 0:
      screensaver_init();
      break;
    case 1:
      page1_init();
      break;
    case 2:
      page2_init();
      break;
    case 3:
      page3_init();
      break;
    case 4:
      page4_init();
      break;
    case 5:
      int change = inserted_amount - inputted_amount.toInt();
      if (change) page5_init();
      break;
    default:
      break;
  }
}

// Page 0: Screensaver screen ----------------------------------
// displays "Coin Change Machine" text with
// blinking Click to continue text
void screensaver_init() {
  lcd1.setCursor(1, 1);
  lcd2.setCursor(1, 1);
  lcd1.print("Coin Change Machine");
  lcd2.print("Coin Change Machine");
  lcd1.setCursor(8, 2);
  lcd2.setCursor(8, 2);
  lcd1.print("****");
  lcd2.print("****");
  lcd1.noCursor();
  lcd2.noCursor();
  lcd1.noBlink();
  lcd2.noBlink();
}
void screensaver() {
  // blinking "click to continue" text
  if (current_millis - lcd_blink_interval > lcd_timer) {
    page1_click_any_to_continue_visible =! page1_click_any_to_continue_visible;
    lcd_timer = current_millis;
  }
  if (page1_click_any_to_continue_visible) {
    lcd1.setCursor(0, 3);
    lcd2.setCursor(0, 3);
    lcd1.print("Click to continue...");
    lcd2.print("Click to continue...");
  } else {
    clear_row(lcd1, 3);
    clear_row(lcd2, 3);
  }
}

// Page 1: Input amount screen   -------------------------------
// screen where the user can input the amount to pay
bool last_is_inputted_amount_valid = true;
void page1_init() {
  lcd1.home();
  lcd2.home();
  lcd1.print("Amount to pay:");
  lcd2.print("Amount to pay:");
  lcd1.setCursor(0, 1);
  lcd2.setCursor(0, 1);
  lcd1.print("PHP ");
  lcd2.print("PHP ");
  lcd1.cursor();
  lcd2.cursor();
  lcd1.blink();
  lcd2.blink();
  set_page1_controls();
  last_is_inputted_amount_valid =! is_inputted_amount_valid;
}
void set_page1_controls(){
  if (is_inputted_amount_valid != last_is_inputted_amount_valid) {
    if (is_inputted_amount_valid) {
      lcd1.setCursor(0, 3);
      lcd2.setCursor(0, 3);
      lcd1.print("A: Erase ");
      lcd2.print("A: Erase ");
      lcd1.setCursor(11, 3);
      lcd2.setCursor(11, 3);
      lcd1.print("D: Submit");
      lcd2.print("D: Submit");
    } else {
      clear_row(lcd1, 3);
      clear_row(lcd2, 3);
      lcd1.setCursor(0, 3);
      lcd2.setCursor(0, 3);
      lcd1.print("A: Cancel");
      lcd2.print("A: Cancel");
      lcd1.setCursor(4, 1);
      lcd2.setCursor(4, 1);
    }
    last_is_inputted_amount_valid = is_inputted_amount_valid;
  }
}
String last_displayed_inputted_amount = " ";
void page1() {
  set_page1_controls();
  // refresh amount to display when amount changed
  if (inputted_amount != last_displayed_inputted_amount) {
    lcd1.setCursor(4, 1);
    lcd2.setCursor(4, 1);
    lcd1.print("      ");
    lcd2.print("      ");
    lcd1.setCursor(4, 1);
    lcd2.setCursor(4, 1);
    lcd1.print(inputted_amount);
    lcd2.print(inputted_amount);
    last_displayed_inputted_amount = inputted_amount;
  }
}

// Page 2: Accepting bill and coin screen ----------------------------
// Screen that displays the inputted amount to pay and
// the amount inserted
void page2_init() {
  lcd1.home();
  lcd2.home();
  lcd1.print("Amount:  PHP ");
  lcd2.print("Amount:  PHP ");
  lcd1.print(inputted_amount);
  lcd2.print(inputted_amount);
  lcd1.setCursor(0, 1);
  lcd2.setCursor(0, 1);
  lcd1.print("Balance: ");
  lcd2.print("Balance: ");
  lcd1.setCursor(0, 3);
  lcd2.setCursor(0, 3);
  lcd1.print("A: Cancel ");
  lcd2.print("A: Cancel ");
  lcd1.noCursor();
  lcd2.noCursor();
  lcd1.noBlink();
  lcd2.noBlink();
}
bool inserted_amount_visible = true;
void page2() {
  if (current_millis - lcd_blink_interval > lcd_timer) {
    inserted_amount_visible =! inserted_amount_visible;
    lcd_timer = current_millis;
  }
  if (inserted_amount_visible) {
    lcd1.setCursor(9, 1);
    lcd2.setCursor(9, 1);
    lcd1.print("PHP ");
    lcd2.print("PHP ");
    lcd1.print(inserted_amount);
    lcd2.print(inserted_amount);
  } else {
    lcd1.setCursor(9, 1);
    lcd2.setCursor(9, 1);
    lcd1.print("         ");
    lcd2.print("         ");
  }
}


// Page 3: Please insert coin/bill prompt screen -------------------------
// screen that displays "Please insert Coin/Bill" text 
// and redirects to Page 2 after 1.5sec
void page3_init() {
  lcd1.setCursor(4, 1);
  lcd2.setCursor(4, 1);
  lcd1.print("Please insert");
  lcd2.print("Please insert");
  lcd1.setCursor(5, 2);
  lcd2.setCursor(5, 2);
  lcd1.print("Coin / Bill");
  lcd2.print("Coin / Bill");
  lcd1.noCursor();
  lcd2.noCursor();
  lcd1.noBlink();
  lcd2.noBlink();
}
void page3() {
  if (current_millis - 1500 > on_amount_submit_millis) {
    transition_page(2);
  }
}


// Page 4: Transaction cancelled screen
// screen that displays "Transaction cancelled" text 
// and redirects to Page 9 - screensaver after 1.5sec
void page4_init() {
  lcd1.setCursor(4, 1);
  lcd2.setCursor(4, 1);
  lcd1.print("Transaction");
  lcd2.print("Transaction");
  lcd1.setCursor(5, 2);
  lcd2.setCursor(5, 2);
  lcd1.print("Cancelled");
  lcd2.print("Cancelled");
  lcd1.noCursor();
  lcd2.noCursor();
  lcd1.noBlink();
  lcd2.noBlink();
}
void page4() {
  if (current_millis - 1500 > cancellation_millis) {
    transition_page(0);
  }
}

// Page 5: Please wait for your change screen
void page5_init() {
  lcd1.setCursor(4, 0);
  lcd2.setCursor(4, 0);
  lcd1.print("Please wait");
  lcd2.print("Please wait");
  lcd1.setCursor(1, 1);
  lcd2.setCursor(1, 1);
  lcd1.print("for your change...");
  lcd2.print("for your change...");
  lcd1.noCursor();
  lcd2.noCursor();
  lcd1.noBlink();
  lcd2.noBlink();
}

void page5() {
  int change = inserted_amount - inputted_amount.toInt();
  if (change) {
    // set balance
    int dispensed = 0;
    if (is_dynamic_algo()) dispensed = alternative_dispense(change);
    else dispensed = greedy_dispense(change);

    // set balance 
    if (change > dispensed) {
      inserted_amount = change - dispensed;
      inputted_amount = "0";
      transition_page(7);
    }
    else {
      if (for_cancellation) {
        transition_page(4);
      } else {
        transition_page(6);
      }
    }
  }
}


// Page 6: Transaction completed screen
// screen that displays "Transaction completed" text 
// and redirects to Page 0 - screensaver after 5sec
void page6() {
  lcd1.setCursor(4, 1);
  lcd2.setCursor(4, 1);
  lcd1.print("Transaction");
  lcd2.print("Transaction");
  lcd1.setCursor(5, 2);
  lcd2.setCursor(5, 2);
  lcd1.print("Completed");
  lcd2.print("Completed");
  lcd1.noCursor();
  lcd2.noCursor();
  lcd1.noBlink();
  lcd2.noBlink();
  if (millis() - 5000 > transaction_completed_millis) {
    transition_page(0);
  }
}


// Page 7: Refill coins screen
// screen that displays "Please refill coins" text 
void page7() {
  // admin
  lcd1.setCursor(4, 1);
  lcd1.print("Please refill");
  lcd1.setCursor(6, 2);
  lcd1.print("Coins...");
  lcd1.setCursor(0, 3);
  lcd1.print("A: Try again ");
  
  // client
  lcd2.setCursor(3, 1);
  lcd2.print("Please contact");
  lcd2.setCursor(4, 2);
  lcd2.print("the Admin...");
  clear_row(lcd2, 3);
  
  lcd1.noCursor();
  lcd2.noCursor();
  lcd1.noBlink();
  lcd2.noBlink();
}
