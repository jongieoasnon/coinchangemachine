/*
 * This file covers the selection of change coin algo used
 */

int algo_switch_pin = 12;

void algo_switch_setup() {
  pinMode(algo_switch_pin, INPUT_PULLUP);
}

bool is_dynamic_algo() {
  return digitalRead(algo_switch_pin);
}
