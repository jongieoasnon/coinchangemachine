/*
 * This file covers the main pages and initializations of the UI
 * Page 0: Screensaver screen
 * Page 1: Input amount screen
 * Page 2: Accepting bill and coin screen
 * Page 3: Please insert coin/bill prompt screen
 * Page 4: Transaction cancelled screen
 * Page 5: Please wait for your change screen
 * Page 6: Transaction completed screen
 */

#include <Wire.h>
#include <LiquidCrystal_I2C.h>
#include <Keypad.h>


LiquidCrystal_I2C lcd1(0x26, 20, 4);
LiquidCrystal_I2C lcd2(0x27, 20, 4);

int page = 0, last_page = 0;
long inactivity_timeout = 10000; 
long last_press = inactivity_timeout * -1, current_millis = 0;


String inputted_amount = "";

void setup() {
	// initializations
  lcd_setup();
  bill_and_coin_acceptor_setup();
  hopper_setup();
  algo_switch_setup();
//  Serial.begin(9600);
}

void loop() {
  current_millis = millis();
  char key = get_pressed_key();

  // display respective page UI and run other function dependencies
  switch(page) {
    case 0:
      listen_screensaver_timeout(key);
      screensaver();
      break;
    case 1:
      listen_screensaver_timeout(key);
      listen_page1_input(key);
      page1();
      break;
    case 2:
      listen_page2_input(key);
      listen_inserted_amount();
      page2();
      break;
    case 3:
      page3();
      break;
    case 4:
      page4();
      break;
    case 5:
      page5();
      break;
    case 6:
      page6();
      break;
    case 7:
      listen_page7_input(key);
      page7();
      break;
    default:
      screensaver();
      break;
  }


  // clear and initialize page when transitioning to another page
  if (page != last_page) {
    clear_lcds();
    page_init(page);
    last_page = page;
  }

  set_acceptors_relay();
}
