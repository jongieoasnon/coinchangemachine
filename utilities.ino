/*
 * This file covers some utility functions mainly for setting variables
 * on every page transition
 */
 
void clear_row(LiquidCrystal_I2C lcd, int row) {
  lcd.setCursor(0, row);
  lcd.print("                    ");
}

void transition_page(int x) {
  switch (x) {
    case 1:
      //  input amount page
      page = 1;
      break;
    case 2:
      // insert money page
      page = 2;
      break;
    case 3:
      // submission of amount 
      on_amount_submit_millis = current_millis;
      page = 3;
      break;
    case 4:
      // cancellation
      page = 4;
      last_press = current_millis - inactivity_timeout;
      cancellation_millis = current_millis;
      inputted_amount = "";
      inserted_amount = 0;
      cursor_pos = 0;
      for_cancellation = false;
      is_inputted_amount_valid = false;
      break;
    case 5:
      // insert amount completed
      page = 5;
      amount_insert_completed_millis = current_millis;
      break;
    case 6:
      // transaction completed
      page = 6;
      inserted_amount = 0;
      inputted_amount = "";
      for_cancellation = false;
      transaction_completed_millis = current_millis;
      break;
    case 7:
      // refill coins
      page = 7;
      break;
    default:
      page = 0;
      cursor_pos = 0;
      inputted_amount = "";
      inserted_amount = 0;
      is_inputted_amount_valid = false;
  }
}
