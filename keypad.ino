/*
 * This file covers the keypad initialization and keypress listening
 */


int cursor_pos = 0;
bool is_inputted_amount_valid = false;
long on_amount_submit_millis = 0, cancellation_millis = 0;
bool for_cancellation = false;

const byte ROWS = 4;
const byte COLS = 4;
char hexaKeys[ROWS][COLS] = {
  {'1', '2', '3', 'A'},
  {'4', '5', '6', 'B'},
  {'7', '8', '9', 'C'},
  {'*', '0', '#', 'D'}
};
byte rowPins[ROWS] = {11, 10, 9, 8};
byte colPins[COLS] = {7, 6, 5, 4};

Keypad customKeypad = Keypad(makeKeymap(hexaKeys), rowPins, colPins, ROWS, COLS);

// returns the pressed key when called
char get_pressed_key() {
  return customKeypad.getKey();
}

void listen_screensaver_timeout(char key) {
  // put to screensaver when inactive for inactivity_timeout<int>
  // wake when pressed
  if (key) last_press = current_millis;
  if (current_millis - inactivity_timeout >= last_press) page = 0;
  else transition_page(1);
}

// functions that listens to user inputs on Page 1
void listen_page1_input(char key) {
  if (isDigit(key) && cursor_pos <= 4) {
    cursor_pos = cursor_pos <= 4 ? cursor_pos+1: cursor_pos;
    inputted_amount.concat(key);
  } else if (key == 'A') {
    if (cursor_pos >= 0) {
      inputted_amount = inputted_amount.substring(0, cursor_pos);
      cursor_pos--;
    } else transition_page(4);
  } else if (key == 'D' && is_inputted_amount_valid) transition_page(3);
  is_inputted_amount_valid = inputted_amount != "00000" && inputted_amount != "";
}

// Function that listens to user inputs on Page 2
// For user cancellation
void listen_page2_input(char key) {
  if (key == 'A') {
    inputted_amount = "";
    for_cancellation = true;
    transition_page(5);
  }
}

// Function that listens to user inputs on Page 7
// For coin dispense try again
void listen_page7_input(char key) {
  if (key == 'A') {
    transition_page(5);
  }
}
