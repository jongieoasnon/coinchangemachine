/*
 * This file covers the coin hopper operations
 */

// setup variables
const int hoppers_control_pins[] = {23, 24, 25};
const int hoppers_sensor_pins[] = {19, 18, 17};
int hoppers_amount[3] = {1, 5, 10};

// operation variables
bool hoppers_status[3] = {false, false, false};
bool last_hoppers_read[] = {false, false, false};
long last_dispense_millis = 0;
int dispensed_amount = 0;

void hopper_setup() {
  // initialize pins
  for(int i=0; i <= 2; i++) {
    pinMode(hoppers_sensor_pins[i], INPUT);
    pinMode(hoppers_control_pins[i], OUTPUT);
  }
  disable_hoppers();
}

void disable_hoppers() {
  for(int i=0; i <= 2; i++) {
    digitalWrite(hoppers_control_pins[i], !false);
  }
}

void reset_hopppers_status() {
  for(int i = 2; i >= 0; i--) {
    hoppers_status[i] = false;
  }
}

void hoppers_listen(int *x) {
  // get millis timestamp
  current_millis = millis();

  // read hoppers
  bool hoppers_read[] = {false, false, false};
  for(int i=0; i <= 2; i++) {
    hoppers_read[i] = digitalRead(hoppers_sensor_pins[i]);
  }

  // detect falling signal and increment dispensed_amount by the dispensed coin value
  for(int i=0; i <= 2; i++) {
    if (hoppers_read[i] != last_hoppers_read[i]) {
      if ((!i && hoppers_read[i]) || (i && !hoppers_read[i])) {
        dispensed_amount += hoppers_amount[i];
        x[i]++;
        last_dispense_millis = current_millis;
      }
    }
    last_hoppers_read[i] = hoppers_read[i];
  }

  // set hoppers status
  for(int i=0; i <= 2; i++) {
    digitalWrite(hoppers_control_pins[i], !hoppers_status[i]);
  }
}


int greedy_dispense(int change) {
  dispensed_amount = 0;
  int active_hopper = 2;
  int dispensed_coins[3] = {0, 0, 0};
  last_dispense_millis = millis();

  // set initial hopper to activate
  // get the next lower denomination hopper
  for(int i = 2; i >= 0; i--) {
    if (hoppers_amount[i] <= change) {
      active_hopper = i;
      break;
    }
  }

  // loop
  while(active_hopper >= 0) {
    // set hopper status
    for(int i = 2; i >= 0; i--) {
        hoppers_status[i] = false;
        hoppers_status[i] = i == active_hopper;
    }

    // hopper listen
    hoppers_listen(dispensed_coins);
    display_remaining(change-dispensed_amount);

    // move to next lower denomination if change left is lower than the current denomination
    if (hoppers_amount[active_hopper] > change - dispensed_amount) {
      active_hopper--;
    }

    // break loop if change is complete
    if (change <= dispensed_amount) {
      reset_hopppers_status();
      disable_hoppers();
      break;
    }

    // move to next lower denomination if no dispensed coin last 2.5seconds
    if (current_millis - 1500 > last_dispense_millis ) {
      if (!active_hopper) disable_hoppers();
      active_hopper--;
      last_dispense_millis = current_millis;
    }
  }

  return dispensed_amount;
}

int alternative_dispense(int change) {
  dispensed_amount = 0;
  int dispensed_coins[3] = {0, 0, 0};
  int coins_count_to_dispense[3] = {0,0,0};
  last_dispense_millis = millis();
  int remaining = change;
  int active_hopper = 2;

  // identify number of coins to be dispensed for each denomination
  while(active_hopper>-1 && remaining) {
    if (hoppers_amount[active_hopper] <= remaining) {
      coins_count_to_dispense[active_hopper]++;
      remaining -= hoppers_amount[active_hopper];
    } else {
      active_hopper--;
    }
  }
  
  // dispense
  while(active_hopper >= 0) {
    
    // set hopper status
    for(int i = 0; i <= 2; i++) {
      hoppers_status[i] = dispensed_coins[i] < coins_count_to_dispense[i] ? true: false;
    }

    // hopper listen
    hoppers_listen(dispensed_coins);
    display_remaining(remaining);

    if (current_millis - 1500 > last_dispense_millis ) {
      disable_hoppers();
      return dispensed_amount;
    }
  }
  return dispensed_amount;
}

int last_remaining = 0;
void display_remaining(int remaining){
  if (remaining != last_remaining) {
    clear_row(lcd1, 3);
    clear_row(lcd1, 3);
    lcd1.setCursor(7, 3);
    lcd2.setCursor(7, 3);
    lcd1.print("PHP ");
    lcd2.print("PHP ");
    lcd1.print(remaining);
    lcd2.print(remaining);
    last_remaining = remaining;
  }
}
